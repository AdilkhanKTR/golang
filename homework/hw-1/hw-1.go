package main

import (
	"fmt"
)

var a, b, c, d int

func main() {
	fmt.Scan(&a, &b, &c, &d)
	max, min := minCheck(a, b, c, d) // Send values to minCheck Function
	fmt.Println("max: ", max)
	fmt.Println("min: ", min)
}

func minCheck(a int, b int, c int, d int) (int, int) { // Min Check function
	dArr := []int{a, b, c, d} // Slice of input values
	max := dArr[0]
	min := dArr[0]
	for _, value := range dArr { // cycle for checking values of slice
		if value > max { // check condition of max
			max = value
		}
		if value < min { // check condition of min
			min = value
		}
	}
	return max, min // return values to main() func
}
