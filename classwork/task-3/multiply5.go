package main

import (
	"fmt"
	"strconv"
)

func main() {
	var a int

	fmt.Scanf("%d", &a)

	if a%5 == 0 && a > 5 {
		a = (a - 5) / 10
		a = a * (a + 1)
		a := strconv.Itoa(a)
		fmt.Println(a + "25")
	} else if a == 5 {
		a *= a
		fmt.Println(a)
	} else {
		fmt.Println("wrong input number")
	}
}
