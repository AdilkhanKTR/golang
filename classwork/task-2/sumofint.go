package main

import (
	"fmt"
)

func main() {
	var a int
	sum := 0
	fmt.Scanf("%d", &a)

	for i := 0; i <= a; i++ {
		sum += i
	}
	if sum < 100000 {
		fmt.Println(sum)
	} else {
		fmt.Println("OverHead")
	}
}
